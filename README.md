# BeFly3

A collection of Matlab modules to analyse rhythmic data

## Installing / Getting started

Make sure Matlab is installed (ideally with the parallel computing toolbox to speed analyses), then install BeFly3 using BeFly.mlappinstall

Detailed graphical step by step instructions in BeFly3_instructions.pdf

## Contributing

Version history

BeFly1 A collection of VBA scripts for Drosophila circadian analysis using TriKinetics DAM file data
BeFly2 An updated collection of VBA scripts for Drosophila circadian and sleep analysis using TriKinetics DAM file data 
BeFly3 Matlab app for advanced circadian analysis of Drosophila activity captured using TriKinetics DAM file data

## Contributing

If you'd like to contribute, please fork the repository and use a feature
branch. Pull requests are warmly welcome.
